#include "parser.h"

struct Number {
    char zeroToNine;
};

const struct Number nTable[] = {
    { '0' }, { '1' }, { '2' }, { '3' },
    { '4' }, { '5' }, { '6' }, { '7' },
    { '8' }, { '9' },
};

bool check::checkNumber (char number){
    for(int i = 0; i < 10; i++){
        if(nTable[i].zeroToNine == number)
            return true;
    }
    return false;
}

struct Arithmetic {
    char arithmetic;
};

const struct Arithmetic aTable [] = {
    { '+' },   { '-' },
    { '*' },   { '/' },
    { '%' },
};

bool check::checkArithmetic (char Arithmetic){
    for(int i = 0; i < 5; i++){
        if(aTable[i].arithmetic == Arithmetic)
            return true;
    }
    return false;
}

struct Letters {
    char Letters_aToZ;
};

const struct Letters lTable [] = {
    { 'a' }, { 'b' }, { 'c' }, { 'd' },
    { 'e' }, { 'f' }, { 'g' }, { 'h' },
    { 'i' }, { 'j' }, { 'k' }, { 'l' },
    { 'm' }, { 'n' }, { 'o' }, { 'p' },
    { 'q' }, { 'r' }, { 's' }, { 't' },
    { 'u' }, { 'v' }, { 'w' }, { 'x' },
    { 'y' }, { 'z' }, { 'A' }, { 'B' },
    { 'C' }, { 'D' }, { 'E' }, { 'F' },
    { 'G' }, { 'H' }, { 'I' }, { 'J' },
    { 'K' }, { 'L' }, { 'M' }, { 'N' },
    { 'O' }, { 'P' }, { 'Q' }, { 'R' },
    { 'S' }, { 'T' }, { 'U' }, { 'V' },
    { 'W' }, { 'X' }, { 'Y' }, { 'Z' },
};

bool check::checkletter(char letter) {
    for(int i = 0; i < 52; i++) {
        if(lTable[i].Letters_aToZ == letter)
            return true;
    }
    return false;
}

bool check::checkID (string token) {
    // Check if the first char of token is letter
    if(checkletter(token[0])) {
        // Check the rest char of token if they are either letter or number
        for(int i = 1; i < token.length(); i++) {
            if(checkletter(token[i]))
                continue;
            else if(checkNumber(token[i]))
                continue;
            else {
                return false;
            }
        }
        return true;
    }
    return false;
}

vector<string> check::checkExp(vector<string> token){
    vector<string> answer(2);
    stringstream ss;
    check *check = new check();
    // Check if the first token is ID
    if (token[0] != " " && check->checkID(token[0])) {
        // Check the next token is Arithmetic Operation
        if (token[1].length() == 1 && check->checkArithmetic(token[1][0])) {
            // Check if the last token is ID
            if (check->checkID(token[token.size()-1])) {
                for (int i = 2; i < token.size()-1; i++) {
                    if (!(check->checkID(token[i]))) {
                        answer[0] = "0";
                        ss << "The token \"" << token[i] << "\" is not a valid ID.";
                        answer[1] = ss.str();
                        return answer;
                    }
                    i = i + 1;
                    if (!(check->checkArithmetic(token[i][0]))) {
                        answer[0] = "0";
                        ss << "The token \"" << token[i] << "\" is not a valid Operator.";
                        answer[1] = ss.str();
                        return answer;
                    }
                }
                answer[0] = "1";
                answer[1] = " ";
                return answer;
            }
            answer[0] = "0";
            ss << "The token \"" << token[token.size()-1] << "\" is not a valid ID.";
            answer[1] = ss.str();
            return answer;
        }
        answer[0] = "0";
        ss << "The token \"" << token[1] << "\" is not a valid Operator.";
        answer[1] = ss.str();
        return answer;
    }
    answer[0] = "0";
    ss << "The token \"" << token[0] << "\" is not a valid ID.";
    answer[1] = ss.str();
    return answer;
}

vector<string> check::checkAssg(vector<string> token) {
    vector<string> answer(2);
    stringstream ss;
    check *check = new check();
    // Check the first token is ID
    if (check->checkID(token[0])) {
        // Check the second token is an equal sign
        if (token[1] == "=") {
            // Check the last token is ;
            if (token[token.size() - 1][0] == ';') {
                vector<string> restOfString;
                for (int i = 2; i < token.size()-1; i++) {
                    restOfString.push_back(token[i]);
                }
                return checkExp(restOfString);
            }
            answer[0] = "0";
            ss << "The last token \"" << token[token.size() - 1] << "\" is not a semi colon.";
            answer[1] = ss.str();
            return answer;
        }
        answer[0] = "0";
        ss << "The second token \"" << token[1] << "\" is not an equal sign.";
        answer[1] = ss.str();
        return answer;
    }
    answer[0] = "0";
    ss << "The first token \"" << token[0] << "\" is not a valid ID.";
    answer[1] = ss.str();
    return answer;
}


